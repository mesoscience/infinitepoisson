#ifndef VOLUME_SET_PHI1_H
#define VOLUME_SET_PHI1_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/node.h"
#include "libmesh/numeric_vector.h"

using namespace libMesh;

// Define values of phi1
void volume_set_phi1(EquationSystems& vm)
{
  // Get a constant reference to the mesh object.
  const MeshBase& mesh_vm = vm.get_mesh();
 
  // Get a reference to the ExplicitSystem
  //ExplicitSystem& system_vm = vm.get_system<ExplicitSystem> ("Partition");
  LinearImplicitSystem& system_vm = vm.get_system<LinearImplicitSystem> ("Poisson");

  // Get system number and variable numbers
  const unsigned short int           system_number = system_vm.number();
  const unsigned short int    variable_number_phi1 = system_vm.variable_number("phi1");

  // Get a constant reference to variable, get their number of components
  const Variable&                    variable_phi1 = system_vm.variable(variable_number_phi1);
  const unsigned short int      variable_comp_phi1 = variable_phi1.n_components();

  // Node iterator
  MeshBase::const_node_iterator           nd = mesh_vm.local_nodes_begin();
  const MeshBase::const_node_iterator end_nd = mesh_vm.local_nodes_end();

  // Loop over all nodes to assign values to Mx My Mz
  for ( ; nd != end_nd ; ++nd){
      const Node* node_vm = *nd;

      // Global dof_index for each node
      const dof_id_type node_dof_index_phi1    = node_vm->dof_number(system_number,
                                                                     variable_number_phi1,
                                                                     variable_comp_phi1-1);
      // Assign values to phi1
      system_vm.solution->set(   node_dof_index_phi1, 1.0);
  }
}

#endif
