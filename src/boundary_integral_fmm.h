#ifndef BOUNDARY_INTEGRAL_FMM_H
#define BOUNDARY_INTEGRAL_FMM_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/explicit_system.h"
#include "libmesh/dof_map.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/node.h"

// PETSc headers
#include "petscsys.h" // PetscLogEvent{Register,Begin,End}

// ScalFMM headers
#include "Utils/FPoint.hpp"
#include "Components/FTypedLeaf.hpp"
#include "Containers/FOctree.hpp"
#include "Kernels/Chebyshev/FChebCell.hpp"
#include "Kernels/Interpolation/FInterpMatrixKernel.hpp"
#include "Kernels/Chebyshev/FChebKernel.hpp"
#include "Kernels/P2P/FP2PParticleContainerIndexed.hpp"
#include "Core/FFmmAlgorithmTsm.hpp"

// infinitepoisson headers
#include "constants.h"

using namespace libMesh;

// Fast multipole accelerated boundary integral
void boundary_integral_fmm(EquationSystems& bs,
                           unsigned int TreeHeight, unsigned int SubTreeHeight,
                           Real boxWidth, Real cx, Real cy, Real cz, Real lx, Real ly, Real lz)
{
  // Get a constant reference to the mesh object.
  const MeshBase& mesh_bs = bs.get_mesh();
 
  // The element dimension of boundary mesh
  const unsigned int dim = mesh_bs.mesh_dimension();

  // Get a reference to the ExplicitSystem 
  ExplicitSystem& system_bs = bs.get_system<ExplicitSystem> ("BoundaryPotential");

  // Petsc performance for localize
  //int LOCA_EVENT;
  //PetscLogEventRegister("Localize",0,&LOCA_EVENT);
  //PetscLogEventBegin(LOCA_EVENT,0,0,0,0);
  // Get a local copy of global solution on each processor
  //std::vector < Number > global_solution_copy;
  //system_bs.solution->localize(global_solution_copy);
  //
  //PetscLogEventEnd(LOCA_EVENT,0,0,0,0);

  std::vector<Number> global_potential(system_bs.solution->size()/2);

  // Petsc performance log for boundary integral calculation
  // other than VecScatter
  int USER_EVENT;
  PetscLogEventRegister("BoundaryInt",0,&USER_EVENT);
  PetscLogEventBegin(USER_EVENT,0,0,0,0);

  // A reference to the DofMap object for this system.  The  DofMap
  // object handles the index translation from node and element numbers
  // to degree of freedom numbers.  
  const DofMap& dof_map_bs = system_bs.get_dof_map();

  // Define dof_indices holder for phi1
  std::vector<dof_id_type> dof_indices_phi1;

  // Get system number and variable numbers
  const unsigned short int        system_number = system_bs.number();
  const unsigned short int variable_number_phi1 = system_bs.variable_number("phi1");
  const unsigned short int variable_number_phi2 = system_bs.variable_number("phi2");

  // Get a constant reference to variable phi1 and phi2, get their number of components
  const Variable&                 variable_phi1 = system_bs.variable(variable_number_phi1);
  const unsigned short int   variable_comp_phi1 = variable_phi1.n_components();

  const Variable&                 variable_phi2 = system_bs.variable(variable_number_phi2);
  const unsigned short int   variable_comp_phi2 = variable_phi2.n_components();

  // Get a constant reference to the Finite Element type
  // for the first (and only) variable in the system.
  FEType bs_type = dof_map_bs.variable_type(variable_number_phi1);

  // Build a Finite Element object of the specified type.  Since the
  // FEBase::build() member dynamically creates memory we will
  // store the object as an UniquePtr<FEBase>.  This can be thought
  // of as a pointer that will clean up after itself. 
  UniquePtr<FEBase> bs_face (FEBase::build(dim, bs_type));

  // quadraure rule for surface integration
  // with dimensionality one less than the dimensionality
  // of the element.
  QGauss qface(dim, FIFTH);

  // Tell FE object to use quadrature rule.
  bs_face->attach_quadrature_rule (&qface);

  // ScalFMM
  typedef double FReal;
  // In order to be used in a template, a constant value must be initialized
  const unsigned int ORDER = 5;
  FPoint<FReal> centerOfBox( cx, cy, cz );

  // Particle, Leaf, Cell, Octree
  typedef FP2PParticleContainerIndexed<FReal>                 ContainerClass;
  typedef FTypedLeaf<FReal,ContainerClass>                    LeafClass;
  typedef FTypedChebCell<FReal,ORDER>                         CellClass;
  typedef FOctree<FReal,CellClass,ContainerClass,LeafClass>   OctreeClass;

  // Kernel D(1/r)/Dx2,D(1/r)/Dy2,D(1/r)/Dz2
  typedef FInterpMatrixKernelRx2<FReal> MatrixKernelClass1;
  typedef FInterpMatrixKernelRy2<FReal> MatrixKernelClass2;
  typedef FInterpMatrixKernelRz2<FReal> MatrixKernelClass3;

  // Three kernel classes
  typedef FChebKernel<FReal,CellClass,ContainerClass,MatrixKernelClass1,ORDER> KernelClass1;
  typedef FChebKernel<FReal,CellClass,ContainerClass,MatrixKernelClass2,ORDER> KernelClass2;
  typedef FChebKernel<FReal,CellClass,ContainerClass,MatrixKernelClass3,ORDER> KernelClass3;

  // Three kernels
  const   MatrixKernelClass1 MatrixKernel1;
  const   MatrixKernelClass2 MatrixKernel2;
  const   MatrixKernelClass3 MatrixKernel3;

  // Timer
  FTic counter;

  // Octrees 
  OctreeClass tree1(TreeHeight,SubTreeHeight,boxWidth,centerOfBox);
  OctreeClass tree2(TreeHeight,SubTreeHeight,boxWidth,centerOfBox);
  OctreeClass tree3(TreeHeight,SubTreeHeight,boxWidth,centerOfBox);

  // Ready to insert particles
  counter.tic();

  FPoint<FReal> particlePosition;
  FSize indexPart = 0;

  // Node iterator for global mesh, because here we don't partition target points.
  MeshBase::const_node_iterator           nd = mesh_bs.nodes_begin();
  const MeshBase::const_node_iterator end_nd = mesh_bs.nodes_end();

  // Loop over all nodes - targets
  for ( ; nd != end_nd ; ++nd){   
      const Node* node_bs = *nd;

      // Target point coords
      const Real xt = (*node_bs)(0);
      const Real yt = (*node_bs)(1);
      const Real zt = (*node_bs)(2);
      particlePosition.setPosition( xt, yt, zt );

      // Insert into trees             particleType    index     physicalValue  pot forces 
      tree1.insert(particlePosition, FParticleType(1), indexPart,           0., 0., 0., 0., 0.);
      tree2.insert(particlePosition, FParticleType(1), indexPart,           0., 0., 0., 0., 0.);
      tree3.insert(particlePosition, FParticleType(1), indexPart,           0., 0., 0., 0., 0.);

      indexPart += 1;
  }

  const FSize nbTargets = indexPart;

  // Iterator el will iterate from the first to the last element on
  // this processor. Because here we partition the source points.
  const MeshBase::const_element_iterator end_el = mesh_bs.local_elements_end();
  MeshBase::const_element_iterator          el  = mesh_bs.local_elements_begin();

  // Loop over all sources at quadrature points in every elements. 
  // ++el requires an unnecessary temporary object.
  for ( ; el != end_el ; ++el){
      // Store a pointer to the element
      const Elem* elem_bs = *el;
  
      // The Jacobian * Quadrature Weight at the quadrature points on the face.
      const std::vector<Real>& JxW_face = bs_face->get_JxW();

      // The shape function at quadrature points
      const std::vector<std::vector<Real> >& phi = bs_face->get_phi();
  
      // The XYZ locations (in physical space) of the quadrature points on the face.
      const std::vector<Point >& qface_point = bs_face->get_xyz();
  
      // Tangent direction of xi and eta, cross product to get normal
      const std::vector<RealGradient >& qface_dxyzdxi  = bs_face->get_dxyzdxi();
      const std::vector<RealGradient >& qface_dxyzdeta = bs_face->get_dxyzdeta();
 
      // Compute the shape function values on the element face.
      bs_face->reinit(elem_bs);

      // Calculate the normal vector, we only need normal vector at one quadrature point because surface element is 2D
      // Cross product
      Real nx = qface_dxyzdxi[0](1)*qface_dxyzdeta[0](2)-qface_dxyzdxi[0](2)*qface_dxyzdeta[0](1);
      Real ny = qface_dxyzdxi[0](2)*qface_dxyzdeta[0](0)-qface_dxyzdxi[0](0)*qface_dxyzdeta[0](2);
      Real nz = qface_dxyzdxi[0](0)*qface_dxyzdeta[0](1)-qface_dxyzdxi[0](1)*qface_dxyzdeta[0](0);

      // Normalize the normal vector
      Real nunit = sqrt (nx*nx + ny*ny +nz*nz);
      nx = nx / nunit;
      ny = ny / nunit;
      nz = nz / nunit;

      // Global dof_indices for this element and variable phi1
      dof_map_bs.dof_indices(elem_bs, dof_indices_phi1, variable_number_phi1);
      // Number of dof indices for phi1 on this element
      // used to loop through all nodes to calculate phi1 on quadrature point
      const unsigned int n_phi1_dofs = dof_indices_phi1.size();

      // Loop over the face quadrature points for integration.
      for (unsigned int qp=0; qp<qface.n_points(); qp++){
          // Location of source points
          const Real x_qp = qface_point[qp](0);
          const Real y_qp = qface_point[qp](1);
          const Real z_qp = qface_point[qp](2);
 
          // Value of phi1 at quadrature point
          Real phi1_qp = 0.0;
          for (unsigned int l=0; l < n_phi1_dofs; l++){
              phi1_qp += phi[l][qp] * (*system_bs.current_local_solution)(dof_indices_phi1[l]);
          }

          Real phys = JxW_face[qp]*phi1_qp;

          particlePosition.setPosition( x_qp , y_qp , z_qp );
          // Insert into trees             particleType    index      physicalValue  pot forces
          tree1.insert(particlePosition, FParticleType(0), indexPart,       phys*nx, 0., 0., 0., 0.);
          tree2.insert(particlePosition, FParticleType(0), indexPart,       phys*ny, 0., 0., 0., 0.);
          tree3.insert(particlePosition, FParticleType(0), indexPart,       phys*nz, 0., 0., 0., 0.);

          indexPart += 1;
      }
  // End of looping over elements
  }

  const FSize nbTotal = indexPart;

  counter.tac();
  std::cout << "Done  " << "(" << counter.elapsed() << "). " << std::endl
            << nbTargets << " target particles, " << nbTotal-nbTargets << " source particles." << std::endl;

  // Apply kernels, here performs the compression and set M2L operators
  KernelClass1 kernel1(TreeHeight,boxWidth,centerOfBox,&MatrixKernel1);
  KernelClass2 kernel2(TreeHeight,boxWidth,centerOfBox,&MatrixKernel2);
  KernelClass3 kernel3(TreeHeight,boxWidth,centerOfBox,&MatrixKernel3);

  // FMM algorithm combine octree and kernel
  typedef FFmmAlgorithmTsm<OctreeClass,CellClass,ContainerClass,KernelClass1,LeafClass> FmmClass1;
  typedef FFmmAlgorithmTsm<OctreeClass,CellClass,ContainerClass,KernelClass2,LeafClass> FmmClass2;
  typedef FFmmAlgorithmTsm<OctreeClass,CellClass,ContainerClass,KernelClass3,LeafClass> FmmClass3;

  FmmClass1 algorithm1(&tree1, &kernel1);
  FmmClass2 algorithm2(&tree2, &kernel2);
  FmmClass3 algorithm3(&tree3, &kernel3);

  std::cout << "Performing D(1/r)/Dn kernel TSM calculation ..." << std::endl;
  counter.tic();
  algorithm1.execute();
  algorithm2.execute();
  algorithm3.execute();
  counter.tac();
  std::cout << "FMM calculation done  " << "(" << counter.elapsed() << ")." << std::endl;

  // Store particle information
  struct TestParticle{
     FReal potential;
  };
  TestParticle* const particles = new TestParticle[nbTotal];

  // Get surface integral at each target point
  tree1.forEachLeaf([&](LeafClass* leaf){
      const FReal*const potentials = leaf->getTargets()->getPotentials();
      const FSize nbParticlesInLeaf = leaf->getTargets()->getNbParticles();
      const FVector<FSize>& indexes = leaf->getTargets()->getIndexes();

      for(FSize idxPart = 0 ; idxPart < nbParticlesInLeaf ; ++idxPart){
          const FSize indexPartOrig = indexes[idxPart];
          particles[indexPartOrig].potential += potentials[idxPart];
          global_potential[indexPartOrig] = potentials[idxPart];
      }
  });

  tree2.forEachLeaf([&](LeafClass* leaf){
      const FReal*const potentials = leaf->getTargets()->getPotentials();
      const FSize nbParticlesInLeaf = leaf->getTargets()->getNbParticles();
      const FVector<FSize>& indexes = leaf->getTargets()->getIndexes();

      for(FSize idxPart = 0 ; idxPart < nbParticlesInLeaf ; ++idxPart){
          const FSize indexPartOrig = indexes[idxPart];
          particles[indexPartOrig].potential += potentials[idxPart];
          global_potential[indexPartOrig] += potentials[idxPart];
      }
  });

  tree3.forEachLeaf([&](LeafClass* leaf){
      const FReal*const potentials = leaf->getTargets()->getPotentials();
      const FSize nbParticlesInLeaf = leaf->getTargets()->getNbParticles();
      const FVector<FSize>& indexes = leaf->getTargets()->getIndexes();

      for(FSize idxPart = 0 ; idxPart < nbParticlesInLeaf ; ++idxPart){
          const FSize indexPartOrig = indexes[idxPart];
          particles[indexPartOrig].potential += potentials[idxPart];
          global_potential[indexPartOrig] += potentials[idxPart];
      }
  });

  // Get contribution from all source points among all processors.
  mesh_bs.comm().sum(global_potential);

  // Reset nd and particle index, assign values to nodes at this processor
  indexPart = 0;
  nd = mesh_bs.local_nodes_begin();
  const MeshBase::const_node_iterator end_nd_local = mesh_bs.local_nodes_end();

  if (lx == 0){ // NOT cuboid.
    for ( ; nd != end_nd_local ; ++nd){
        const Node* node_bs = *nd;
  
        // Dof_index for each node
        const dof_id_type node_dof_index_phi1 = node_bs->dof_number(system_number,
                                                                    variable_number_phi1,
                                                                    variable_comp_phi1-1);
  
        const dof_id_type node_dof_index_phi2 = node_bs->dof_number(system_number,
                                                                    variable_number_phi2,
                                                                    variable_comp_phi2-1);
  
        // Boundary integral value to solution vector, minus SIGN get (R_target-R_source) in ScalFMM
        //system_bs.solution->set(node_dof_index_phi2, -particles[indexPart].potential/PI_4
        system_bs.solution->set(node_dof_index_phi2, -global_potential[node_bs->id()]/PI_4
                                                     -1./2.*(*system_bs.solution)(node_dof_index_phi1));
  
        indexPart += 1;
    }
  } else { // Cuboid.
    for ( ; nd != end_nd_local ; ++nd){
        const Node* node_bs = *nd;

        // Node's coordinates.
        const Real xt = (*node_bs)(0);
        const Real yt = (*node_bs)(1);
        const Real zt = (*node_bs)(2);
 
        // Dof_index for each node
        const dof_id_type node_dof_index_phi1 = node_bs->dof_number(system_number,
                                                                    variable_number_phi1,
                                                                    variable_comp_phi1-1);
  
        const dof_id_type node_dof_index_phi2 = node_bs->dof_number(system_number,
                                                                    variable_number_phi2,
                                                                    variable_comp_phi2-1);

        // Coefficient depends on solid angle.
        Real omega;

        // Determine point is on surface, edge or corner of the cuboid.
        if ( (xt == -lx/2.)||(xt == lx/2.) ){
           if ( (yt == -ly/2.)||(yt == ly/2.) ){
              if ( (-lz/2.<zt)&&(zt<lz/2.) ) {omega=1./4.;} // Edge.
              else {omega=1./8.;} // Corner.
           } else {
              if ( (-lz/2.<zt)&&(zt<lz/2.) ) {omega=1./2.;} // Surface.
              else {omega=1./4.;} // Edges.
           }
        } else {
           if ( ((yt == -ly/2.)||(yt == ly/2.)) && ((zt == -lz/2.)||(zt == lz/2.)) ){omega=1./4.;} // Edge.
           else {omega=1./2.;} // Surface.
        }
 
        // Boundary integral value to solution vector, minus SIGN get (R_target-R_source) in ScalFMM
        //system_bs.solution->set(node_dof_index_phi2, -particles[indexPart].potential/PI_4
          system_bs.solution->set(node_dof_index_phi2, -global_potential[node_bs->id()]/PI_4
                                                       -(1.-omega)*(*system_bs.solution)(node_dof_index_phi1));

        indexPart += 1;
    }
  }

  // Petsc performance log
  PetscLogEventEnd(USER_EVENT,0,0,0,0);
// End of function Boundary Integral FMM
}

#endif
