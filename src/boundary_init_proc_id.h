#ifndef BOUNDARY_INIT_PROC_ID_H
#define BOUNDARY_INIT_PROC_ID_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/explicit_system.h"
#include "libmesh/node.h"
#include "libmesh/numeric_vector.h"

using namespace libMesh;

// Assign proc_id on surface. See partition pattern.
void boundary_init_proc_id(EquationSystems& bs,
                           const Parallel::Communicator& comm)
{
  // Get a constant reference to the mesh object.
  const MeshBase& mesh_bs = bs.get_mesh();
 
  // Get a reference to the ExplicitSystem
  ExplicitSystem& system_bs = bs.get_system<ExplicitSystem> ("BoundaryPotential");

  // Get system number and variable numbers
  const unsigned short int           system_number = system_bs.number();
  const unsigned short int variable_number_proc_id = system_bs.variable_number("proc_id");

  // Get a constant reference to variable proc_id, get their number of components
  const Variable&                 variable_proc_id = system_bs.variable(variable_number_proc_id);
  const unsigned short int   variable_comp_proc_id = variable_proc_id.n_components();

  // Node iterator
  MeshBase::const_node_iterator           nd = mesh_bs.local_nodes_begin();
  const MeshBase::const_node_iterator end_nd = mesh_bs.local_nodes_end();

  // Loop over all nodes to assign values to proc_id
  for ( ; nd != end_nd ; ++nd){
      const Node* node_bs = *nd;

      // Global dof_index for each node
      const dof_id_type node_dof_index_proc_id = node_bs->dof_number(system_number,
                                                                     variable_number_proc_id,
                                                                     variable_comp_proc_id-1);
      // Assign cpu_id to proc_id
      system_bs.solution->set(node_dof_index_proc_id, comm.rank());
  }
}

#endif
