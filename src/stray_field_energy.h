#ifndef STRAY_FIELD_ENERGY_H
#define STRAY_FIELD_ENERGY_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/dof_map.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/numeric_vector.h"

// infinitepoisson headers
#include "evaluate_M.h"

using namespace libMesh;

void stray_energy(EquationSystems& es,
                  const std::string& system_name)
{
  // make sure we are assembling the proper system.
  libmesh_assert_equal_to (system_name, "Combine");

  // Declare a performance log.
  PerfLog perf_log ("Stray Field Energy");

  // Get a constant reference to the mesh object.
  const MeshBase& mesh = es.get_mesh();

  // The dimension that we are running
  const unsigned int dim = mesh.mesh_dimension();

  // Get a reference to the LinearImplicitSystem we are solving
  LinearImplicitSystem& system = es.get_system<LinearImplicitSystem>("Combine");

  const unsigned short int variable_number_phi = system.variable_number("phi");

  // A reference to the DofMap object.  The DofMap object handles the index translation from 
  // node and element numbers to degree of freedom numbers.
  const DofMap& dof_map = system.get_dof_map();

  // Constant reference to the Finite Element type for variable phi2.
  FEType fe_type = dof_map.variable_type(0);

  // Build a Finite Element object of the specified type.
  UniquePtr<FEBase> fe (FEBase::build(dim, fe_type));

  // A 5th order Gauss quadrature rule for numerical integration.
  QGauss qrule (dim, fe_type.default_quadrature_order());

  // Tell the finite element object to use our quadrature rule.
  fe->attach_quadrature_rule (&qrule);

  // Element Jacobian * quadrature weight at each integration point.
  const std::vector<Real>& JxW = fe->get_JxW();

  // The physical XYZ locations of the quadrature points on the element.
  const std::vector<Point>& q_point = fe->get_xyz();

  // The element shape function gradients evaluated at the quadrature points.
  const std::vector<std::vector<Real> >& dphidx       = fe->get_dphidx();
  const std::vector<std::vector<Real> >& dphidy       = fe->get_dphidy();
  const std::vector<std::vector<Real> >& dphidz       = fe->get_dphidz();

  // Degree of freedom indices for the element.
  std::vector<dof_id_type> dof_indices;

  Real strayEnergy = 0.;

  // Loop over all the elements in the mesh.
  MeshBase::const_element_iterator       el     = mesh.active_local_elements_begin();
  const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();

  for ( ; el != end_el; ++el){
      // Store a pointer to the element we are currently working on.
      const Elem* elem = *el;

      // Degree of freedom indices for the current element.
      dof_map.dof_indices (elem, dof_indices, variable_number_phi);

      // Number of dof indices for phi1 on this element
      // used to loop through all nodes to calculate dphi on quadrature point
      const unsigned int n_phi_dofs = dof_indices.size();

      // Compute the element-specific data for the current element.
      fe->reinit (elem);

      // Energy calculation.
      for (unsigned int qp=0; qp<qrule.n_points(); qp++){

          const Real x = q_point[qp](0);
          const Real y = q_point[qp](1);
          const Real z = q_point[qp](2);

          const Real Mx = eval_Mx(x,y,z);
          const Real My = eval_My(x,y,z);
          const Real Mz = eval_Mz(x,y,z);

          // Value of phi1 at quadrature point
          Real dphi_qp = 0.0;
          for (unsigned int l=0; l < n_phi_dofs; l++){
              dphi_qp += ( Mx*dphidx[l][qp] + My*dphidy[l][qp] + Mz*dphidz[l][qp]) * (*system.current_local_solution)(dof_indices[l]);
          }

          strayEnergy += JxW[qp]*dphi_qp;
     }
  }

  mesh.comm().sum(strayEnergy);

  std::cout << "Stray field energy is " << strayEnergy*0.5 << std::endl;
}

#endif
