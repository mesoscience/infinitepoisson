#ifndef BOUNDARY_INTEGRAL_DIRECT_H
#define BOUNDARY_INTEGRAL_DIRECT_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/explicit_system.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/dof_map.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/node.h"

// infinitepoisson headers
#include "constants.h"

// PETSc headers
#include "petscsys.h" // PetscLogEvent{Register,Begin,End}

using namespace libMesh;

// Direct method for boundary integral on surface.
void boundary_integral_direct(EquationSystems& bs,
                              Real eps, Real /*lx*/, Real /*ly*/, Real /*lz*/)
{
  // Get a constant reference to the mesh object.
  const MeshBase& mesh_bs = bs.get_mesh();
 
  // The element dimension of boundary mesh
  const unsigned int dim = mesh_bs.mesh_dimension();

  // Get a reference to the ExplicitSystem 
  ExplicitSystem& system_bs = bs.get_system<ExplicitSystem> ("BoundaryPotential");

  // Petsc performance for localize
  int LOCA_EVENT;
  PetscLogEventRegister("Localize",0,&LOCA_EVENT);
  PetscLogEventBegin(LOCA_EVENT,0,0,0,0);
  // Get a local copy of global solution on each processor
  std::vector < Number > global_solution_copy;
  system_bs.solution->localize(global_solution_copy);
  //
  PetscLogEventEnd(LOCA_EVENT,0,0,0,0);

  // Petsc performance log for boundary integral calculation
  // other than VecScatter
  int USER_EVENT;
  PetscLogEventRegister("BoundaryInt",0,&USER_EVENT);
  PetscLogEventBegin(USER_EVENT,0,0,0,0);

  // A reference to the DofMap object for this system.  The  DofMap
  // object handles the index translation from node and element numbers
  // to degree of freedom numbers.  
  const DofMap& dof_map_bs = system_bs.get_dof_map();

  // Define dof_indices holder for phi1
  std::vector<dof_id_type> dof_indices_phi1;

  // Get system number and variable numbers
  const unsigned short int        system_number = system_bs.number();
  const unsigned short int variable_number_phi1 = system_bs.variable_number("phi1");
  const unsigned short int variable_number_phi2 = system_bs.variable_number("phi2");

  // Get a constant reference to variable phi1 and phi2, get their number of components
  const Variable&                 variable_phi1 = system_bs.variable(variable_number_phi1);
  const unsigned short int   variable_comp_phi1 = variable_phi1.n_components();

  const Variable&                 variable_phi2 = system_bs.variable(variable_number_phi2);
  const unsigned short int   variable_comp_phi2 = variable_phi2.n_components();

  // Get a constant reference to the Finite Element type
  // for the first (and only) variable in the system.
  FEType bs_type = dof_map_bs.variable_type(variable_number_phi1);

  // Build a Finite Element object of the specified type.  Since the
  // FEBase::build() member dynamically creates memory we will
  // store the object as an UniquePtr<FEBase>.  This can be thought
  // of as a pointer that will clean up after itself.  Introduction Example 4
  // describes some advantages of  UniquePtr's in the context of
  // quadrature rules.
  UniquePtr<FEBase> bs_face (FEBase::build(dim, bs_type));

  // quadraure rule for surface integration
  // with dimensionality one less than the dimensionality
  // of the element.
  QGauss qface(dim, FIFTH);

  // Tell FE object to use quadrature rule.
  bs_face->attach_quadrature_rule (&qface);

  // Iterator el will iterate from the first to the last element on
  // all processors. Because the boundary integral needs all elements.
  const MeshBase::const_element_iterator end_el = mesh_bs.active_elements_end();

  // Node iterator for local processor
  MeshBase::const_node_iterator           nd = mesh_bs.local_nodes_begin();
  const MeshBase::const_node_iterator end_nd = mesh_bs.local_nodes_end();

  // Loop over all nodes
  for ( ; nd != end_nd ; ++nd){
      const Node* node_bs = *nd;

      // Dof_index for each node
      const dof_id_type node_dof_index_phi1 = node_bs->dof_number(system_number,
                                                                  variable_number_phi1,
                                                                  variable_comp_phi1-1);

      const dof_id_type node_dof_index_phi2 = node_bs->dof_number(system_number,
                                                                  variable_number_phi2,
                                                                   variable_comp_phi2-1);

      // Target point coords
      const Real xt = (*node_bs)(0);
      const Real yt = (*node_bs)(1);
      const Real zt = (*node_bs)(2);

      // Initial value for integration 
      Real BI = 0.0;
    
      // Reset the first element of all processors
      MeshBase::const_element_iterator  el  = mesh_bs.active_elements_begin();
    
      // Loop over all sources at quadrature points in every elements. 
      // ++el requires an unnecessary temporary object.
      for ( ; el != end_el ; ++el)
         {
          // Store a pointer to the element we are currently
          // working on.  This allows for nicer syntax later.
          const Elem* elem_bs = *el;
    
          // The Jacobian * Quadrature Weight at the quadrature
          // points on the face.
          const std::vector<Real>& JxW_face = bs_face->get_JxW();

          // The shape function at quadrature points
          const std::vector<std::vector<Real> >& phi = bs_face->get_phi();
    
          // The XYZ locations (in physical space) of the
          // quadrature points on the face.
          const std::vector<Point >& qface_point = bs_face->get_xyz();
    
          // Tangent direction of xi and eta, cross product to get normal
          const std::vector<RealGradient >& qface_dxyzdxi  = bs_face->get_dxyzdxi();
          const std::vector<RealGradient >& qface_dxyzdeta = bs_face->get_dxyzdeta();
 
          // Compute the shape function values on the element face.
          bs_face->reinit(elem_bs);

          // Calculate the normal vector, we only need normal vector at one quadrature point
          // because surface element is 2D
          // Cross product
          Real nx = qface_dxyzdxi[0](1)*qface_dxyzdeta[0](2)-qface_dxyzdxi[0](2)*qface_dxyzdeta[0](1);
          Real ny = qface_dxyzdxi[0](2)*qface_dxyzdeta[0](0)-qface_dxyzdxi[0](0)*qface_dxyzdeta[0](2);
          Real nz = qface_dxyzdxi[0](0)*qface_dxyzdeta[0](1)-qface_dxyzdxi[0](1)*qface_dxyzdeta[0](0);

          // Normalize the normal vector
          Real nunit = sqrt (nx*nx + ny*ny +nz*nz);
          nx = nx / nunit;
          ny = ny / nunit;
          nz = nz / nunit;

          // Global dof_indices for this element and variable phi1
          dof_map_bs.dof_indices(elem_bs, dof_indices_phi1, variable_number_phi1);
          // Number of dof indices for phi1 on this element
          // used to loop through all nodes to calculate phi1 on quadrature point
          const unsigned int n_phi1_dofs = dof_indices_phi1.size();

          // Loop over the face quadrature points for integration.
          for (unsigned int qp=0; qp<qface.n_points(); qp++)
             {
              // Difference between location of source and target points
              const Real dx = xt-qface_point[qp](0);
              const Real dy = yt-qface_point[qp](1);
              const Real dz = zt-qface_point[qp](2);
    
              // Distance between source and target points and its power 2
              const Real st2 = dx*dx + dy*dy + dz*dz;
              const Real st  = sqrt(st2);
    
              // Kernel in the boundary integration
              const Real Kxy = ( st < eps ) ? 0.0 : (dx*nx + dy*ny + dz*nz)/(st2*st);

              // Value of phi1 at quadrature point
              Real phi1_qp = 0.0;
              for (unsigned int l=0; l < n_phi1_dofs; l++)
                 {
                  phi1_qp += phi[l][qp] * global_solution_copy[dof_indices_phi1[l]];
                 }
              // The value of integration
              BI += JxW_face[qp]*Kxy*phi1_qp;
             }
          // End of quadrature point loop,
         }
      // Boundary integral value to solution vector
      system_bs.solution->set(node_dof_index_phi2, BI/PI_4
                                                  -1./2.*(*system_bs.solution)(node_dof_index_phi1));
     }

  // Petsc performance log
  PetscLogEventEnd(USER_EVENT,0,0,0,0);
// End of function Boundary Integral Direct
}

#endif
