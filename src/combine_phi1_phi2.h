#ifndef COMBINE_PHI1_PHI2_H
#define COMBINE_PHI1_PHI2_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/dof_map.h"
#include "libmesh/numeric_vector.h"

using namespace libMesh;

void combine_phi1_phi2(EquationSystems& vm){
  const MeshBase& mesh = vm.get_mesh();

  LinearImplicitSystem& system_poisson = vm.get_system<LinearImplicitSystem> ("Poisson");
  LinearImplicitSystem& system_laplace = vm.get_system<LinearImplicitSystem> ("Laplace");
  ExplicitSystem&       system_combine = vm.get_system<ExplicitSystem> ("Combine");

  // DofMap for BoundaryMesh
  const DofMap& dof_map_poisson = system_poisson.get_dof_map();
  const DofMap& dof_map_laplace = system_laplace.get_dof_map();
  const DofMap& dof_map_combine = system_combine.get_dof_map();

  // Get system number and variable numbers
  const unsigned short int  variable_number_phi1 = system_poisson.variable_number("phi1");
  const unsigned short int  variable_number_phi2 = system_laplace.variable_number("phi2");
  const unsigned short int  variable_number_phi  = system_combine.variable_number("phi");

  // In order to get solution vector
  std::vector<dof_id_type> dof_indices_phi1, dof_indices_phi2, dof_indices_phi;
  std::vector<Real> values;

  // Get dof indices for phi1 & phi2 for all nodes on this element
  dof_map_poisson.local_variable_indices(dof_indices_phi1, mesh, variable_number_phi1);
  dof_map_laplace.local_variable_indices(dof_indices_phi2, mesh, variable_number_phi2);
  dof_map_combine.local_variable_indices(dof_indices_phi , mesh, variable_number_phi);

  // Get values
  system_poisson.solution->get(dof_indices_phi1,values);
  system_combine.solution->add_vector(values,dof_indices_phi);

  system_laplace.solution->get(dof_indices_phi2,values);
  system_combine.solution->add_vector(values,dof_indices_phi);
}

#endif
