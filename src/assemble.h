#ifndef ASSEMBLE_H
#define ASSEMBLE_H

// libMesh headers
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/dof_map.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/elem.h"
#include "libmesh/numeric_vector.h"

// infinitepoisson headers
#include "constants.h"
#include "evaluate_M.h"

using namespace libMesh;

void assemble_poisson(EquationSystems& es,
                      const std::string& system_name)
{
  // make sure we are assembling the proper system.
  libmesh_assert_equal_to (system_name, "Poisson");

  // Declare a performance log.
  PerfLog perf_log ("Matrix Assembly");

  // Get a constant reference to the mesh object.
  const MeshBase& mesh = es.get_mesh();

  // The dimension that we are running
  const unsigned int dim = mesh.mesh_dimension();

  // Get a reference to the LinearImplicitSystem we are solving
  LinearImplicitSystem& system = es.get_system<LinearImplicitSystem>("Poisson");

  // A reference to the DofMap object.  The DofMap object handles the index translation from 
  // node and element numbers to degree of freedom numbers.
  const DofMap& dof_map = system.get_dof_map();

  // Constant reference to the Finite Element type for variable phi1.
  FEType fe_type = dof_map.variable_type(0);

  // Build a Finite Element object of the specified type.
  UniquePtr<FEBase> fe (FEBase::build(dim, fe_type));

  // A 5th order Gauss quadrature rule for numerical integration.
  QGauss qrule (dim, fe_type.default_quadrature_order());

  // Tell the finite element object to use our quadrature rule.
  fe->attach_quadrature_rule (&qrule);

  // Declare a special finite element object for boundary integration.
  UniquePtr<FEBase> fe_face (FEBase::build(dim, fe_type));

  // Boundary integration requires one quadraure rule,
  // with dimensionality one less than the dimensionality of the element.
  QGauss qface(dim-1, fe_type.default_quadrature_order());
  fe_face->attach_quadrature_rule (&qface);

  // Element Jacobian * quadrature weight at each integration point.
  const std::vector<Real>& JxW = fe->get_JxW();

  // The physical XYZ locations of the quadrature points on the element.
  const std::vector<Point>& q_point = fe->get_xyz();

  // The element shape functions evaluated at the quadrature points.
  const std::vector<std::vector<Real> >& phi = fe->get_phi();

  // The element shape function gradients evaluated at the quadrature points.
  const std::vector<std::vector<RealGradient> >& dphi = fe->get_dphi();
  const std::vector<std::vector<Real> >& dphidx       = fe->get_dphidx();
  const std::vector<std::vector<Real> >& dphidy       = fe->get_dphidy();
  const std::vector<std::vector<Real> >& dphidz       = fe->get_dphidz();

  // Data structures to contain the element matrix and right-hand-side vector.
  DenseMatrix<Number> Ke;
  DenseVector<Number> Fe;

  // Degree of freedom indices for the element. These define where in the global system
  // the element degrees of freedom get mapped.
  std::vector<dof_id_type> dof_indices;

  // Loop over all the elements in the mesh.
  MeshBase::const_element_iterator       el     = mesh.active_local_elements_begin();
  const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();

  for( ; el != end_el; ++el){
      // Start logging the shape function initialization.
      perf_log.push("elem init");

      // Store a pointer to the element we are currently working on.
      const Elem* elem = *el;

      // Degree of freedom indices for the current element.  These define where in the global
      // matrix and right-hand-side this element will contribute to.
      dof_map.dof_indices (elem, dof_indices);

      // Compute the element-specific data for the current element. This involves computing the location of the
      // quadrature points (q_point) and the shape functions (phi, dphi) for the current element.
      fe->reinit (elem);

      // Zero the element matrix and right-hand side before summing them.  We use the resize member here because
      // the number of degrees of freedom might have changed from the last element. Note that this will be the case if the
      // element type is different (i.e. the last element was a triangle, now is a quadrilateral).
      Ke.resize (dof_indices.size(),
                 dof_indices.size());

      Fe.resize (dof_indices.size());

      // Stop logging the shape function initialization.
      perf_log.pop("elem init");

      // Now we will build the element matrix.  This involves
      // a double loop to integrate the test funcions (i) against
      // the trial functions (j) for Poisson equation.
      //
      // We have split the numeric integration into two loops
      // so that we can log the matrix and right-hand-side
      // computation seperately.
      //
      // Now start logging the element matrix computation
      perf_log.push ("Ke");

      for (unsigned int qp=0; qp<qrule.n_points(); qp++)
        for (unsigned int i=0; i<phi.size(); i++)
          for (unsigned int j=0; j<phi.size(); j++)
            Ke(i,j) += JxW[qp]*(dphi[i][qp]*dphi[j][qp]);

      // Stop logging the matrix computation
      perf_log.pop ("Ke");

      // Now right-hand-side contribution. This involves a single loop where we integrate the
      // magnetization vector  against the test functions.
      //
      // Start logging the right-hand-side computation
      perf_log.push ("Fe");

      for (unsigned int qp=0; qp<qrule.n_points(); qp++){
          // Mx My Mz are magnetization vector for the Poisson equation.
          //
          // Since the value of magnetization depends only
          // on the location of the quadrature point (q_point[qp])
          // we will compute it outside of the i-loop
          const Real x = q_point[qp](0);
          const Real y = q_point[qp](1);
          const Real z = q_point[qp](2);

          const Real Mx = eval_Mx(x,y,z);
          const Real My = eval_My(x,y,z);
          const Real Mz = eval_Mz(x,y,z);

          // Add the RHS contribution
          for (unsigned int i=0; i<phi.size(); i++)
               Fe(i) += PI_4*JxW[qp]*( Mx*dphidx[i][qp]
                                     + My*dphidy[i][qp]
                                     + Mz*dphidz[i][qp]);
      }

      // Stop logging the right-hand-side computation.
      perf_log.pop ("Fe");

      // If this assembly program were to be used on an adaptive mesh,
      // we would have to apply any hanging node constraint equations.
      dof_map.constrain_element_matrix_and_vector (Ke, Fe, dof_indices);

      // The element matrix and right-hand-side are now built
      // for this element. Add them to the global matrix and
      // right-hand-side vector.
      // Start logging the insertion of the local (element)
      // matrix and vector into the global matrix and vector.
      perf_log.push ("matrix insertion");

      system.matrix->add_matrix (Ke, dof_indices);
      system.rhs->add_vector    (Fe, dof_indices);

      perf_log.pop ("matrix insertion");
    }
}

void assemble_laplace(EquationSystems& es,
                      const std::string& system_name)
{
  // make sure we are assembling the proper system.
  libmesh_assert_equal_to (system_name, "Laplace");

  // Declare a performance log.
  PerfLog perf_log ("Matrix Assembly");

  // Get a constant reference to the mesh object.
  const MeshBase& mesh = es.get_mesh();

  // The dimension that we are running
  const unsigned int dim = mesh.mesh_dimension();

  // Get a reference to the LinearImplicitSystem we are solving
  LinearImplicitSystem& system = es.get_system<LinearImplicitSystem>("Laplace");

  const unsigned short int variable_number_phi2 = system.variable_number("phi2");

  // A reference to the DofMap object.  The DofMap object handles the index translation from 
  // node and element numbers to degree of freedom numbers.
  const DofMap& dof_map = system.get_dof_map();

  // Constant reference to the Finite Element type for variable phi2.
  FEType fe_type = dof_map.variable_type(0);

  // Build a Finite Element object of the specified type.
  UniquePtr<FEBase> fe (FEBase::build(dim, fe_type));

  // A 5th order Gauss quadrature rule for numerical integration.
  QGauss qrule (dim, fe_type.default_quadrature_order());

  // Tell the finite element object to use our quadrature rule.
  fe->attach_quadrature_rule (&qrule);

  // Declare a special finite element object for boundary integration.
  UniquePtr<FEBase> fe_face (FEBase::build(dim, fe_type));

  // Boundary integration requires one quadraure rule,
  // with dimensionality one less than the dimensionality of the element.
  QGauss qface(dim-1, fe_type.default_quadrature_order());
  fe_face->attach_quadrature_rule (&qface);

  // Element Jacobian * quadrature weight at each integration point.
  const std::vector<Real>& JxW = fe->get_JxW();

  // The physical XYZ locations of the quadrature points on the element.
  //const std::vector<Point>& q_point = fe->get_xyz();

  // The element shape functions evaluated at the quadrature points.
  const std::vector<std::vector<Real> >& phi = fe->get_phi();

  // The element shape function gradients evaluated at the quadrature points.
  const std::vector<std::vector<RealGradient> >& dphi = fe->get_dphi();

  // Data structures to contain the element matrix and right-hand-side vector.
  DenseMatrix<Number> Ke;
  DenseVector<Number> Fe;

  // Degree of freedom indices for the element. These define where in the global system
  // the element degrees of freedom get mapped.
  std::vector<dof_id_type> dof_indices;

  // In order to get solution vectors
  std::vector<dof_id_type> dof_indices_phi2;
  std::vector<Real> value;

  // Loop over all the elements in the mesh.
  MeshBase::const_element_iterator       el     = mesh.active_local_elements_begin();
  const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();

  for ( ; el != end_el; ++el)
    {
      // Start logging the shape function initialization.
      perf_log.push("elem init");

      // Store a pointer to the element we are currently working on.
      const Elem* elem = *el;

      // Degree of freedom indices for the current element.  These define where in the global
      // matrix and right-hand-side this element will contribute to.
      dof_map.dof_indices (elem, dof_indices);
      // Global dof_indices for this element and variable phi2
      //dof_map.dof_indices (elem, dof_indices_phi2, variable_number_phi2);

      // Number of dof indices for phi2 on this element
      // used to loop through all nodes to calculate phi2 on quadrature point
      //const unsigned int n_phi2_dofs = dof_indices_phi2.size();
      // Get values from BoundaryMesh for this element
      //system.current_local_solution->get(dof_indices_phi2, value);
 
      // Compute the element-specific data for the current element. This involves computing the location of the
      // quadrature points (q_point) and the shape functions (phi, dphi) for the current element.
      fe->reinit (elem);

      // Zero the element matrix and right-hand side before summing them.  We use the resize member here because
      // the number of degrees of freedom might have changed from the last element. Note that this will be the case if the
      // element type is different (i.e. the last element was a triangle, now is a quadrilateral).
      Ke.resize (dof_indices.size(),
                 dof_indices.size());

      Fe.resize (dof_indices.size());

      // Stop logging the shape function initialization.
      perf_log.pop("elem init");

      // Now we will build the element matrix.  This involves
      // a double loop to integrate the test funcions (i) against
      // the trial functions (j) for Poisson equation.
      //
      // We have split the numeric integration into two loops
      // so that we can log the matrix and right-hand-side
      // computation seperately.
      //
      // Now start logging the element matrix computation
      perf_log.push ("Ke");

      for (unsigned int qp=0; qp<qrule.n_points(); qp++)
        for (unsigned int i=0; i<phi.size(); i++)
          for (unsigned int j=0; j<phi.size(); j++)
            Ke(i,j) += JxW[qp]*(dphi[i][qp]*dphi[j][qp]);

      // Stop logging the matrix computation
      perf_log.pop ("Ke");

      // Penalty method for Dirichlet BC
      // Find the boundary elements
      for (unsigned int side=0; side < elem->n_sides(); side++){
        if (elem->neighbor(side) == NULL){

          const std::vector<std::vector<Real>>& phi_face = fe_face->get_phi();
          const std::vector<Real>&              JxW_face = fe_face->get_JxW();
          const std::vector<Point >&         qface_point = fe_face->get_xyz();

          // Compute the shape function values on the element face.
          fe_face->reinit(elem, side);

          // Loop over the face quadrature points for integration.
          for (unsigned int qp=0; qp<qface.n_points(); qp++){

              // The penalty value. \frac{1}{\epsilon}
              const Real penalty = 1.e10;

              // Coordinates of quadrature point on boundary
              Point p (qface_point[qp](0), qface_point[qp](1), qface_point[qp](2));

              // Value of phi2 at quadrature point on boundary
              Real phi2_qp = system.point_value(variable_number_phi2, p, *elem);

              // Matrix contribution of the L2 projection.
              for (unsigned int i=0; i<phi_face.size(); i++)
                for (unsigned int j=0; j<phi_face.size(); j++)
                  Ke(i,j) += JxW_face[qp]*penalty*phi_face[i][qp]*phi_face[j][qp];

              // Right-hand-side contribution of the L2 projection.
              for (unsigned int i=0; i<phi_face.size(); i++)
                Fe(i) += JxW_face[qp]*penalty*phi2_qp*phi_face[i][qp];
          }
        }
      }

      // If this assembly program were to be used on an adaptive mesh,
      // we would have to apply any hanging node constraint equations.
      dof_map.constrain_element_matrix_and_vector (Ke, Fe, dof_indices);

      // Add element matrix and right-hand-side to the global matrix and
      // right-hand-side vector.
      perf_log.push ("matrix insertion");
      system.matrix->add_matrix (Ke, dof_indices);
      system.rhs->add_vector    (Fe, dof_indices);
      perf_log.pop ("matrix insertion");
    }
}

#endif
