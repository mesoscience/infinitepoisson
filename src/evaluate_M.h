#ifndef EVALUATE_M_H
#define EVALUATE_M_H

#include "libmesh/libmesh_common.h" // Real

using libMesh::Real;

Real eval_Mx(const Real /*x*/, const Real /*y*/, const Real /*z*/){
  return 1.0;
}

Real eval_My(const Real /*x*/, const Real /*y*/, const Real /*z*/){
  return 0.0;
}

Real eval_Mz(const Real /*x*/, const Real /*y*/, const Real /*z*/){
  return 0.0;
}

#endif
