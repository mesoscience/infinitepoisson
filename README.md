### Contact ###
* Xikai Jiang (xikai@anl.gov)

* Email me for help or if you have any questions or comments.

* Our method paper has details on the implementation and tests of this code: [An O(N) and parallel approach to integral problems by a kernel-independent fast multipole method: Application to polarization and magnetization of interacting particles](https://arxiv.org/abs/1605.01715)


### License ###

* The codes are open-source and distributed under the GNU GPL license, and may not be used for any commercial or for-profit purposes without our permission.